//
//  ColorCell.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 03/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "ColorCell.h"

@implementation ColorCell

- (void)setColor:(NSString *)colorCode {
    UIColor *color = [self colorFromHexString:colorCode];
    self.backgroundColor = color;
    self.colorHex.text = [NSString stringWithFormat:@"#%@",colorCode];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
//    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
