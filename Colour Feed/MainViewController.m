//
//  ViewController.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "MainViewController.h"
#import <AFNetworking.h>
#import "PaletteCell.h"
#import "Palette+CoreDataProperties.h"
#import "Palette+Category.h"
#import "AppDelegate.h"
#import "PaletteViewController.h"
#import "ProgressHUD.h"

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *_palettes;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _palettes = [Palette getCachedObjects];
    
    [self loadDataFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataFromServer {
    [ProgressHUD show:@"Please wait..."];
    
    NSString *URLString = @"http://www.colourlovers.com/api/palettes/new";
    NSDictionary *parameters = @{@"format": @"json"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        
        NSArray *objects = responseObject;
        if (objects) {
            
            NSMutableArray *palettes = [NSMutableArray new];
            AppDelegate *appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
            
            for (NSDictionary *object in objects) {
                Palette *palette = [Palette getPaletteWithId:[object[@"id"] integerValue]];
                palette.dateCreated = object[@"dateCreated"];
                palette.colors = object[@"colors"];
                
                palette.imageUrl = object[@"imageUrl"];
                palette.likes = [object[@"numHearts"] integerValue];
                palette.title = object[@"title"];
                palette.userName = object[@"userName"];
                palette.views = [object[@"numViews"] integerValue];
                palette.desc = object[@"description"];
                
                [palettes addObject:palette];
            }
            
            [appDelegate saveContext];
            
            _palettes = palettes;
            [_tableView reloadData];
        }
//        self.navigationItem.title = @"Online Mode";
        
        [ProgressHUD dismiss];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error load date for server: %@", error);
        
        if ([error.domain isEqualToString:NSURLErrorDomain] && (error.code == NSURLErrorNotConnectedToInternet)) {
//            self.navigationItem.title = @"Offline Mode";
            [self showAlertWithText:@"Нет подключения к сети."];
        } else {
            [self showAlertWithText:@"Не удалось подключится к серверу. Попробуйте позже."];
        }
        
        [ProgressHUD dismiss];
    }];
}

- (void)showAlertWithText:(NSString*)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:text preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:(UIAlertActionStyleDefault) handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)reload:(id)sender {
    [self loadDataFromServer];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_palettes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PaletteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaletteCell"];
    Palette *item = _palettes[indexPath.row];
    [cell populateWith:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PaletteViewController *paletteVC = (PaletteViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PaletteScreen"];
    
    if (paletteVC) {
        Palette *item = _palettes[indexPath.row];
        paletteVC.palette = item;
        [self.navigationController pushViewController:paletteVC animated:YES];
    }
    
}

@end
