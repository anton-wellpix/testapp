//
//  PaletteViewController.h
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Palette;

@interface PaletteViewController : UIViewController

@property (nonatomic, weak) Palette *palette;

@end
