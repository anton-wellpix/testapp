//
//  Palette+Category.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 03/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "Palette+Category.h"
#import "Palette+CoreDataProperties.h"
#import "AppDelegate.h"

@implementation Palette (Category)

+ (Palette *)getPaletteWithId:(int64_t)paleteId {
    
    AppDelegate *appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSArray *fetchedObjects;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Palette"  inManagedObjectContext: context];
    [fetch setEntity:entity];
    [fetch setPredicate:[NSPredicate predicateWithFormat:@"id == %d",paleteId]];
    NSError * error = nil;
    fetchedObjects = [context executeFetchRequest:fetch error:&error];
    
    if([fetchedObjects count] == 1)
        return [fetchedObjects objectAtIndex:0];
    else {
        Palette *palette = (Palette *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
        palette.id = paleteId;
        return palette;
    }
}

+ (NSArray *)getCachedObjects {
    AppDelegate *appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
    NSManagedObjectContext *managedContext = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest<Palette *> * fetchRequest = [Palette fetchRequest];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dateCreated" ascending:NO];
    [fetchRequest setSortDescriptors: @[sort]];
    [fetchRequest setFetchLimit:20];
    
    NSError *error = nil;
    NSArray *objects = [managedContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Error paletter fetch: %@", error);
        return nil;
    }
    
    return objects;
}

@end
