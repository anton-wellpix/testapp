//
//  PaletteCell.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "PaletteCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "Palette+CoreDataProperties.h"

@implementation PaletteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)populateWith:(Palette *) palette {
    [_palettePreview sd_setImageWithURL:[NSURL URLWithString:palette.imageUrl]];
    _paletteTitle.text = palette.title;
    _userName.text = palette.userName;
    _likesCount.text = [NSString stringWithFormat:@"%d", palette.likes];
    _viewsCount.text = [NSString stringWithFormat:@"%d", palette.views];
}

@end
