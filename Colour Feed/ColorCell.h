//
//  ColorCell.h
//  Colour Feed
//
//  Created by Anton Boyarkin on 03/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *colorHex;

- (void)setColor:(NSString *)colorCode;

@end
