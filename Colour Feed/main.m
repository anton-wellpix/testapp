//
//  main.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
