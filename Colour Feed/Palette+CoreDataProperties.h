//
//  Palette+CoreDataProperties.h
//  
//
//  Created by Anton Boyarkin on 03/02/2018.
//
//

#import "Palette+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Palette (CoreDataProperties)

+ (NSFetchRequest<Palette *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSArray *colors;
@property (nullable, nonatomic, copy) NSString *dateCreated;
@property (nonatomic) int64_t id;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nonatomic) int16_t likes;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nonatomic) int16_t views;
@property (nullable, nonatomic, copy) NSString *desc;

@end

NS_ASSUME_NONNULL_END
