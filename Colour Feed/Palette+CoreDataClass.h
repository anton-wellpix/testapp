//
//  Palette+CoreDataClass.h
//  
//
//  Created by Anton Boyarkin on 03/02/2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface Palette : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Palette+CoreDataProperties.h"
