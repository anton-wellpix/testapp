//
//  Palette+CoreDataProperties.m
//  
//
//  Created by Anton Boyarkin on 03/02/2018.
//
//

#import "Palette+CoreDataProperties.h"

@implementation Palette (CoreDataProperties)

+ (NSFetchRequest<Palette *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Palette"];
}

@dynamic colors;
@dynamic dateCreated;
@dynamic id;
@dynamic imageUrl;
@dynamic likes;
@dynamic title;
@dynamic userName;
@dynamic views;
@dynamic desc;

@end
