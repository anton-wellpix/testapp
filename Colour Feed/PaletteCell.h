//
//  PaletteCell.h
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Palette;

@interface PaletteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *palettePreview;
@property (weak, nonatomic) IBOutlet UILabel *paletteTitle;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *likesCount;
@property (weak, nonatomic) IBOutlet UILabel *viewsCount;

- (void)populateWith:(Palette *) object;

@end
