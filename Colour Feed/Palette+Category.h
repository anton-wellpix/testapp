//
//  Palette+Category.h
//  Colour Feed
//
//  Created by Anton Boyarkin on 03/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "Palette+CoreDataClass.h"

@interface Palette (Category)

+ (Palette *)getPaletteWithId:(int64_t)paleteId;
+ (NSArray *)getCachedObjects;

@end
