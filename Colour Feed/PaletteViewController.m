//
//  PaletteViewController.m
//  Colour Feed
//
//  Created by Anton Boyarkin on 02/02/2018.
//  Copyright © 2018 AB Software. All rights reserved.
//

#import "PaletteViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Palette+CoreDataProperties.h"
#import "ColorCell.h"

@interface PaletteViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *preview;
@property (weak, nonatomic) IBOutlet UILabel *paletteTitle;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *createdDate;
@property (weak, nonatomic) IBOutlet UILabel *likesCount;
@property (weak, nonatomic) IBOutlet UILabel *viewsCount;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextView *paletteDescription;

@end

@implementation PaletteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_palette) {
        [_preview sd_setImageWithURL:[NSURL URLWithString:_palette.imageUrl]];
        
        self.paletteTitle.text = _palette.title;
        self.userName.text = _palette.userName;
        self.likesCount.text = [NSString stringWithFormat:@"%d", _palette.likes];
        self.viewsCount.text = [NSString stringWithFormat:@"%d", _palette.views];
        self.createdDate.text = _palette.dateCreated;
        self.paletteDescription.text = _palette.desc;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _palette?_palette.colors.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ColorCell *cell = (ColorCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ColorCell" forIndexPath:indexPath];
    NSString *color = _palette.colors[indexPath.item];
    [cell setColor:color];
    return cell;
}

@end
